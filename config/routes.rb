Rails.application.routes.draw do
  mount Peek::Railtie => '/peek'

  devise_for :users
  
  root 'learn#index'

  get 'feedback', to: 'feedback#index', as: :feedback
  get 'learn', to: 'learn#index', as: :learn
  get 'practice', to: 'skills#practice', as: :practice

  get '/test_group_page/:group' => 'test_group_pages#show', as: :test_group_page
  get '/test_group_pages' => 'test_group_pages#index', as: :test_group_pages

  resources :exercises, only: [:index, :show] do
    member do
      post 'evaluate'
    end
  end

  resources :units, only: [:index, :show]
  resources :resources, only: [:show]
  resources :learner, only: [:update]
  resources :learner_logs, only: [:destroy]
  resources :user_activity_states, only: [:create]
  resources :user_activity_logs, only: [:create]


  # applications
  resources :participant_applications do
    member do
      get 'confirmation', as: :confirmation
    end
  end

  get 'application', to: 'participant_applications#new'

  # user
  resources :users

end
