# frozen_string_literal: true
Rails.application.config.after_initialize do
  unless Rails.env.test?

    participants = [
      {
        test_group: User::GROUP_VISUAL,
        users: [
          {
            email: 'visual'
          },
          {
            email: 'nutzer1',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer2',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer3',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer4',
            name: '',
            real_email: ''
          },
        ]
      },
      {
        test_group: User::GROUP_HYBRID,
        users: [
          {
            email: 'hybrid'
          },
          {
            email: 'nutzer5',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer6',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer7',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer8',
            name: '',
            real_email: ''
          },
        ]
      },
      {
        test_group: User::GROUP_TEXTUAL,
        users: [
          {
            email: 'textual'
          },
          {
            email: 'nutzer9',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer10',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer11',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer12',
            name: '',
            real_email: ''
          },
        ]
      },

      {
        test_group: User::GROUP_NO_FEEDBACK,
        users: [
          {
            email: 'no_feedback',
          },
          {
            email: 'nutzer13',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer14',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer15',
            name: '',
            real_email: ''
          },
          {
            email: 'nutzer16',
            name: '',
            real_email: ''
          },
        ]
      },
    ]

    mail_host = "knowyourlanguage.de"
    password = "123456"

    participants.each do |group|
      group[:users].each do |user|
        user = User.find_or_create_by email: "#{user[:email]}@#{mail_host}", name: user[:name], real_email: user[:real_email], password: password, password_confirmation: password, test_group: group[:test_group]
        puts "User created: #{user.email} (#{user.test_group})"
      end
    end

    user = User.find_or_create_by email: "admin@admin.de", password: "123456", password_confirmation: "123456", admin: true
    puts "User created: #{user.email} (#{user.test_group})"

    user = User.find_or_create_by email: "admin2@admin.de", password: "123456", password_confirmation: "123456", admin: true
    puts "User created: #{user.email} (#{user.test_group})"
  end
end
  