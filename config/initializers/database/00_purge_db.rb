# frozen_string_literal: true
Rails.application.config.after_initialize do
  unless Rails.env.test?
    # Mongoid.purge!
  end
end
