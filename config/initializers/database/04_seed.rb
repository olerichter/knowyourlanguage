# # frozen_string_literal: true
# Rails.application.config.after_initialize do
#   unless Rails.env.test?
#     user = User.find_by(email: 'admin@admin.de')
    
#     20.times.map{ 1 + Random.rand(30) }.sort.reverse.each do |n|
#       Timecop.travel(n.days.ago) do
#         exercise = Exercise.skip(rand(Exercise.count)).first
        
#           params = {
#             exercise: exercise,
#             user: user,
#             evaluation_params: {
#               points: (0..10).to_a.sample,
#               misconceptions: [],
#               max_points: 10
#             }
#           }
        
#           Exercises::EvaluationService.evaluate params
#           puts '.'
#       end  
#     end
    
#     20.times.map{ 1 + Random.rand(120) }.sort.reverse.each do |n|
#       Timecop.travel(n.minutes.ago) do
#         exercise = Exercise.skip(rand(Exercise.count)).first
      
#         params = {
#           exercise: exercise,
#           user: user,
#           evaluation_params: {
#             points: (0..10).to_a.sample,
#             misconceptions: [],
#             max_points: 10
#           }
#         }
      
#         Exercises::EvaluationService.evaluate params
#         puts '.'
#       end  
#     end    
#   end
# end

