# frozen_string_literal: true
Rails.application.config.after_initialize do
  unless Rails.env.test?
    Dir.glob(Rails.root.join('lib', 'database', 'parser', '*_skills.json')).each do |file|
      json_skills = File.read(file)
      skills = JSON.parse(json_skills, symbolize_names: true)

      load_skills skills
    end

    Dir.glob(Rails.root.join('lib', 'database', 'parser', '*_exercises.json')).each do |file|
      json_exercises = File.read(file)
      exercises = JSON.parse(json_exercises, symbolize_names: true)

      exercises.each_with_index do |exercise, i|
        load_exercise exercise.merge({order_no: i})
      end
    end

    Dir.glob(Rails.root.join('lib', 'database', 'parser', '*_resources.json')).each do |file|
      json_resources = File.read(file)
      resources = JSON.parse(json_resources, symbolize_names: true)

      resources.each_with_index do |resource, i|
        load_resource(resource, i)
      end
    end

    Resource.all.each do |resource|
      resource.delete unless resource.unit.category.name == "Kurs"
    end

    Exercise.all.each do |exercise|
      # exercise.delete if exercise.resources.count == 0
    end

    Skill.all.each do |skill|
      if skill.exercises.count == 0 && skill.children.count == 0
        skill.delete 
        puts "skill deleted (#{skill.name})"
      end
    end
  end
end

def load_skills(skills)
  params = {
    skills: skills
  }

  create_skills params
end

def load_exercise(exercise)  
  exercise_params = {
    wp_id: exercise[:id],
    type: exercise[:type],
    content: exercise[:content],
    config: exercise[:config],
    name: exercise[:name],
    order_no: exercise[:order_no]
  }

  if Exercise.where(wp_id: exercise[:id]).exists?
    e = Exercise.find_by(wp_id: exercise[:id])
    e.update! exercise_params

    puts "Exercise updated: #{e._type}"

    return
  end

  e = case exercise[:type]
      when Exercise::TYPE_CLOZE
        Exercise::ClozeExercise.new exercise_params
      when Exercise::TYPE_MULTIPLE_CHOICE
        Exercise::ClozeExercise.new exercise_params
      when Exercise::TYPE_WORDLIST
        Exercise::WordlistExercise.new exercise_params
      when Exercise::TYPE_DRAG_DROP
        Exercise::DragDropExercise.new exercise_params
      when Exercise::TYPE_DRAG_DROP_LIST
        Exercise::DragDropListExercise.new exercise_params
      when Exercise::TYPE_SELF_EVALUATION
        Exercise::SelfEvaluationExercise.new exercise_params
      else
        puts "Warning: Exercise skipped #{exercise_params[:wp_id]}"
        Exercise.new exercise_params
      end

  e.skills = create_skills exercise

  e.skills.each do |s|
    s.exercises << e
  end
  e.level = create_level exercise
  e.niveau = create_niveau exercise
  e.save!

  puts "Exercise created: #{exercise[:type]} - #{exercise[:id]}"
end

def load_resource(resource, order_no)
  if Resource.where(wp_id: resource[:id]).exists?
    p = Resource.find_by(wp_id: resource[:id])
    p.update wp_id: resource[:id],
              content: resource[:content],
              name: resource[:name],
              order_no: order_no

    puts "Resource updated: #{p.name}"

    return
  end

  p = Resource.new      wp_id: resource[:id],
                    content: resource[:content],
                    name: resource[:name],
                    order_no: order_no

  # load exercises
  exercises = []
  resource[:exercises].each_with_index do |exercise_id, i|
    wp_id = exercise_id[0, 10]
    exercise = Exercise.find_by(wp_id: /^#{wp_id}/)
    exercise.resources << p
    exercise.save!

    p.exercises << exercise
  end

  # load unit
  (category_name, unit_name) = resource[:unit_name].split(' - ', 2)
  unit_type = (category_name == "Kurs" ? UnitCategory::COURSE_TYPE : UnitCategory::EXERCISE_TYPE)

  c = UnitCategory.where(name: category_name, type: unit_type).first || UnitCategory.create!(name: category_name, type: unit_type)
  u = Unit.where(name: unit_name).first || Unit.create(name: unit_name, resource_ids: [], category: c)

  p.unit = p
  u.resources << p

  u.save!
  p.save!

  puts "Resource created: #{p.name} - #{u.name}"
end

def create_niveau(params)
  Niveau.find_or_create_by(name: params[:niveau])
end

def create_level(params)
  Level.find_or_create_by(name: params[:level])
end

def create_skills(params)
  skills = []

  params[:skills].each do |s|
    if s[:category]
      category = SkillCategory.find_or_create_by(name: CGI.unescapeHTML(s[:category]))

      skill_names = category.name
      unless s[:name].nil? || s[:name].empty?
        skill_names += ' - ' + CGI.unescapeHTML(s[:name])
      end

      skill_tree = skill_names.split ' - '

      skill = find_or_create_skill(category, nil, skill_tree)

      skills << skill
    else
      puts 'Error: missing category'
    end
  end

  skills
end

def find_or_create_skill(category, _child, skill_tree)
  skill_name = skill_tree.pop

  if skill_name
    skill = Skill.where(name: skill_name, category: category).first
    unless skill
      skill = Skill.create(name: skill_name, category: category)
    end

    unless skill_tree.empty?
      parent = find_or_create_skill(category, skill, skill_tree)

      skill.parent = parent

      parent.children << skill
      parent.save!
      skill.save!
    end
  end
  
  skill
end
