require 'mina/rails'
require 'mina/git'
require 'mina/rvm'

set :application_name, 'knowyourlanguage'
set :domain, 'vm-05.cses.informatik.hu-berlin.de'
set :deploy_to, '/home/deployer'
set :repository, 'https://bitbucket.org/olerichter/knowyourlanguage'
set :branch, 'master'

# Username in the server to SSH to.
set :user, 'root'

# shared dirs and files will be symlinked into the app-folder by the
# Not sure if this is necessary
# 'deploy:link_shared_paths' step.
# set :shared_dirs, fetch(:shared_dirs, []).push('log', 'tmp/pids', 'tmp/sockets', 'public/uploads')
# set :shared_files, fetch(:shared_files, []).push('config/database.yml', 'config/secrets.yml', 'config/puma.rb')

task :environment do
  invoke :'rvm:use', 'ruby-2.4.1@default'
end

task :setup do
  %w(database.yml secrets.yml puma.rb).each { |f| command %(touch "#{fetch(:shared_path)}/config/#{f}") }
  comment "Be sure to edit #{fetch(:shared_path)}/config/database.yml, secrets.yml and puma.rb."
end

desc 'Deploys the current version to the server.'
task :deploy do
  deploy do
    comment "Deploying #{fetch(:application_name)} to #{fetch(:domain)}:#{fetch(:deploy_to)}"

    invoke :'git:clone'

    comment 'Cleaning up Docker builds'
    command 'docker stop $(docker ps -qa)'
    command 'docker rm $(docker ps -qa)'

    # comment 'Cleaning all docker networks'
    # command 'docker network prune'

    comment 'Stopping Docker'
    command 'docker-compose stop'

    comment 'Starting Docker'
    command 'docker-compose up -d; sleep 5'

    invoke :'deploy:cleanup'
  end
end
