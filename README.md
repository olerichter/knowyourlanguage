# README

## build instructions
### With Docker
* Install Docker
* Provide .env file in app directory (see .env.example)
* Run 'docker-compose up' in app directory
* Open 'http://localhost:3000/' in your browser

### Without Docker
* Install Ruby, Rails, and mongodb
* start mongodb
* run 'bundle install' and 'rails s' in app directory
* Open 'http://localhost:3000/' in your browser
