FROM ruby:2.3.2-slim
LABEL APP_NAME=knowyourlanguage

ENV APP_NAME=knowyourlanguage LANG=C.UTF-8 LANGUAGE=C.UTF-8 LC_ALL=C.UTF-8 LC_CTYPE=C.UTF-8

RUN apt-get update -qq && apt-get install -y build-essential nodejs npm nodejs-legacy
# RUN apt-get update -qq && apt-get install -y build-essential    
RUN mkdir /app
WORKDIR /app
ADD Gemfile /app/Gemfile
ADD Gemfile.lock /app/Gemfile.lock

ENV BUNDLE_GEMFILE=Gemfile \
    BUNDLE_JOBS=2 \
    BUNDLE_PATH=/bundle \
    RAILS_ENV=development
    
RUN bundle install
ADD . /app
