module Feedback
  module FeedbackHelper
    def exercise_name(exercise)
      exercise.best_resource.name
    end
  end
end
