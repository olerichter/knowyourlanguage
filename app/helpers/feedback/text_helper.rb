module Feedback
  module TextHelper
    def learner_lowest_points(count)
      learner.all.order_by(:points.asc).select {|l| l.points < l.skill.average }[0..count-1]
    end

    def learner_lowest_repetition_score(count)
      # learner.all.order_by(:repetition_score.desc).select{|l| l.repetition_score <= 50 && !l.skill.children.any?}[0..count-1]
      learner.order_by(:updated_at.asc)[0..count-1]
    end

    def new_skills(count)
      Skill.not_in(id: learner.map{|l|l.skill.id}).order_by(:created_at.asc).select{|s| !s.children.any?}[0..count-1]
    end

    private

    def learner
      Learner::Learner.where(user: current_user).includes(:skill)
    end
  end
end
