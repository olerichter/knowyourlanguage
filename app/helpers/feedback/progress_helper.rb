module Feedback
  module ProgressHelper
    DEFAULT_METRIC = 'points'.freeze

    def bar_chart_data(category = nil)
      learner = if category
                  @learner.in_category category
                else
                  @learner.where(:skill_id.in => Skill.roots.map(&:id))
                end

      labels = learner.map { |e| e.skill.name }
      datasets = [{
        label: "Punkte",
        data: learner.map(&:points),
        backgroundColor:  score_background_color(learner),
        borderColor: score_border_color(learner),
        borderWidth: 1
      }]

      { labels: labels, datasets: datasets }.to_json
    end

    def history_line_data_main(metric, scale, skill_id = nil)
      metric ||= DEFAULT_METRIC

      learner = @learner.where(skill_id: skill_id)
      history_line_data(metric, scale, learner)
    end

    def history_line_data_children(metric, scale, skill_id = nil)
      metric ||= DEFAULT_METRIC

      skill = Skill.where(id: skill_id).first
        
      learner = nil
      if skill && skill.children.any?
        learner = @learner.where(:skill_id.in => skill.children.map(&:id))
      else
        learner = @learner.where(:skill_id.in => Skill.roots.map(&:id))
      end
      
      return history_line_data(metric, scale, learner)
    end

    def history_line_data(metric, scale, learner)
      labels = []
      datasets = []

      unless learner.nil? or learner.empty?
        if Integer(scale) > 0
          start_date = Integer(scale).seconds.ago
        else
          start_date = @learner_logs.last.created_at
        end
        end_date = DateTime.now

        step = nice_step(start_date, end_date)

        labels = LearnersQuery.enumerate_dates(start_date, end_date, step).map { |l| "vor #{time_ago_in_words l}" }
        datasets = []

        learner.each_with_index do |learner, i|
          datasets << {
            label: learner.skill.name,
            data: LearnersQuery.history_steady(learner, step, start_date, end_date).map { |e| e[:snapshot] ? e[:snapshot].send(metric) : 0 },
            backgroundColor: dataset_background_colors[i],
            borderColor: dataset_background_colors[i],
            pointBackgroundColor: dataset_background_colors[i],
            fill: false
          }
        end
      end

      { labels: labels, datasets: datasets }.to_json
    end


    def history_line_data_even(metric)
      metric ||= DEFAULT_METRIC
      learner = @learner.where(:skill_id.in => Skill.roots.map(&:id))

      return if learner.count == 0

      start_date = @learner.order_by(:created_at.asc).first.created_at
      end_date = DateTime.now

      step = nice_step(start_date, end_date)

      labels = LearnersQuery.enumerate_dates(start_date, end_date, step).map { |l| "vor #{time_ago_in_words l}" }
      datasets = []
      learner.each_with_index do |learner, i|
        datasets << {
          label: learner.skill.name,
          data: LearnersQuery.history_steady(learner, step, start_date, end_date).map { |e| e[:snapshot] ? e[:snapshot].send(metric) : 0 },
          backgroundColor: dataset_background_colors[i],
          borderColor: dataset_background_colors[i],
          pointBackgroundColor: dataset_background_colors[i],
          fill: false
        }
      end

      { labels: labels, datasets: datasets }.to_json
    end

    private

    def score_background_color_single(points)
      if points < 50
        red = (255 - 0.1 * points * 255 / 50).round(0)
        green = (points * 255 / 50).round(0)
        blue = '0'
      else
        red = (255 - 0.7 * points * 255 / 100).round(0)
        green = 200 + (points * 55 / 100).round(0)
        green = '170'
        blue = '30'
      end

      "rgba(#{red}, #{green}, #{blue}, 0.9)"
    end

    def score_background_color(learner)
      learner.map do |e|
        if e.points < 50
          red = (255 - 0.1 * e.points * 255 / 50).round(0)
          green = (e.points * 255 / 50).round(0)
          blue = '0'
        else
          red = (255 - 0.7 * e.points * 255 / 100).round(0)
          green = 200 + (e.points * 55 / 100).round(0)
          green = '170'
          blue = '30'
        end
    
        "rgba(#{red}, #{green}, #{blue}, 0.4)"
      end
    end

    def score_border_color(learner)
      learner.map do |e|
        if e.points < 50
          red = (255 - 0.1 * e.points * 255 / 50).round(0)
          green = (e.points * 255 / 50).round(0)
          blue = '0'
        else
          red = (255 - 0.7 * e.points * 255 / 100).round(0)
          green = 200 + (e.points * 55 / 100).round(0)
          green = '170'
          blue = '30'
        end

        "rgba(#{red}, #{green}, #{blue}, 1)"
      end
    end

    private
    

    def dataset_background_colors
      [
        'rgba(255, 225, 25, 0.8)',
        'rgba(0, 130, 200, 0.8)',
        'rgba(245, 130, 48, 0.8)',
        'rgba(145, 30, 180, 0.8)',
        'rgba(70, 240, 240, 0.8)',
        'rgba(240, 50, 230, 0.8)',
        'rgba(210, 245, 60, 0.8)',
        'rgba(250, 190, 190, 0.8)',
        'rgba(0, 128, 128, 0.8)',
        'rgba(60, 180, 75, 0.8)',      
        'rgba(230, 190, 255, 0.8)',
        'rgba(170, 110, 40, 0.8)',
        'rgba(255, 250, 200, 0.8)',
        'rgba(128, 0, 0, 0.8)',
        'rgba(230, 25, 75, 0.8)',      
        'rgba(170, 255, 195, 0.8)',
        'rgba(128, 128, 0, 0.8)',
        'rgba(255, 215, 180, 0.8)',
        'rgba(0, 0, 128, 0.8)',
        'rgba(128, 128, 128, 0.8)',

        'rgba(230, 25, 75, 0.8)',
        'rgba(60, 180, 75, 0.8)',
        'rgba(255, 225, 25, 0.8)',
        'rgba(0, 130, 200, 0.8)',
        'rgba(245, 130, 48, 0.8)',
        'rgba(145, 30, 180, 0.8)',
        'rgba(70, 240, 240, 0.8)'
      ]
    end

    def dataset_border_colors
      [
        'rgba(255, 225, 25, 1)',
        'rgba(0, 130, 200, 1)',
        'rgba(245, 130, 48, 1)',
        'rgba(145, 30, 180, 1)',
        'rgba(70, 240, 240, 1)',
        'rgba(240, 50, 230, 1)',
        'rgba(210, 245, 60, 1)',
        'rgba(250, 190, 190, 1)',
        'rgba(0, 128, 128, 1)',
        'rgba(60, 180, 75, 1)',      
        'rgba(230, 190, 255, 1)',
        'rgba(170, 110, 40, 1)',
        'rgba(255, 250, 200, 1)',
        'rgba(128, 0, 0, 1)',
        'rgba(230, 25, 75, 1)',      
        'rgba(170, 255, 195, 1)',
        'rgba(128, 128, 0, 1)',
        'rgba(255, 215, 180, 1)',
        'rgba(0, 0, 128, 1)',
        'rgba(128, 128, 128, 1)',

        'rgba(230, 25, 75, 1)',
        'rgba(60, 180, 75, 1)',
        'rgba(255, 225, 25, 1)',
        'rgba(0, 130, 200, 1)',
        'rgba(245, 130, 48, 1)',
        'rgba(145, 30, 180, 1)',
        'rgba(70, 240, 240, 1)'
      ]
    end

    def nice_step(start_date, end_date)
      (end_date.to_time - start_date.to_time) / 10
    end
  end
end
