class UserActivityLog
  include Mongoid::Document
  include Mongoid::Timestamps

  OPEN_EXERCISE_FROM_FEEDBACK_EVENT = "open_exercise_from_feedback"
  HYBRID_FEEDBACK_CHANGE_TAB = "hybrid_feedback_change_tab"


  field :event_type, type: String
  field :detail, type: String

  belongs_to :user
end
