class Niveau
  include Mongoid::Document
  include Mongoid::Timestamps

  field :order_no, type: Integer
  field :name, type: String

  has_many :exercises
end
