module Scorable
  extend ActiveSupport::Concern

  included do
    field :points, type: Integer
    field :progress, type: Integer
    field :misconceptions, type: Array
  end
  
  # def score
  #   max_exercises = learner.skill.exercises.count
  #   0.5 * [1, completed_exercises.count / max_exercises].max + 0.5 * points / 100
  # end

  def progress_percentage
    # 100 * completed_exercises.count / learner.skill.exercises.count
    10
  end
end
