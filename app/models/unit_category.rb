class UnitCategory
  include Mongoid::Document
  include Mongoid::Timestamps

  COURSE_TYPE = "course"
  EXERCISE_TYPE = "exercise"

  field :name, type: String
  field :type, type: String

  has_many :units

  scope :courses, -> {where(type: COURSE_TYPE)}
  scope :exercises, -> {where(type: EXERCISE_TYPE)}
  
  def skills
    units.map{|u| u.skills}.flatten.uniq
  end

  def skills_to_string
    skills.map {|s| s.to_string_detailed }.sort
  end
end

