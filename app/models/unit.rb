class Unit
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  belongs_to :category, class_name: "UnitCategory"  
  has_many :resources

  def skills
    resources.map {|r| r.exercises.map {|e| e.skills }}.flatten.uniq
  end
end
