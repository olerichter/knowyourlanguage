class Exercise
  include Mongoid::Document
  include Mongoid::Timestamps

  TYPE_CLOZE = 'cloze'.freeze
  TYPE_MULTIPLE_CHOICE = 'multiple_choice'.freeze
  TYPE_DRAG_DROP_LIST = 'drag_drop_list'.freeze
  TYPE_DRAG_DROP = 'drag_drop'.freeze
  TYPE_TEXT_SPOTTING = 'text_spotting'.freeze
  TYPE_WORDLIST = 'wordlist'.freeze
  TYPE_SELF_EVALUATION = 'self_evaluation'.freeze

  validates :type, presence: true

  field :wp_id, type: String
  field :type, type: String
  field :content, type: String
  field :config, type: Hash
  field :name, type: String
  field :order_no, type: Integer

  has_and_belongs_to_many :skills, validate: false
  belongs_to :niveau
  belongs_to :level

  has_and_belongs_to_many :resources, validate: false

  def name
    "Übung #{order_no}"
  end

  def parsed_content
    JSON.parse(content, symbolize_names: true, object_class: OpenStruct)
  end

  def best_resource
    resources.last
  end
end
