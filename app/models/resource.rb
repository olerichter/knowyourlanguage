class Resource
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::SortedRelations

  field :name, type: String
  field :wp_id, type: String
  field :content, type: String

  field :order_no, type: Integer

  belongs_to :unit
  has_and_belongs_to_many :exercises

  def next_resource
    next_resource = Resource.where(order_no: order_no + 1).first
    if next_resource && next_resource.unit.category.type == UnitCategory::COURSE_TYPE and unit.category.type == UnitCategory::COURSE_TYPE
      next_resource
    else
      nil
    end
  end

  def prior_resource
    before_resource = Resource.where(order_no: order_no - 1).first
    if before_resource && before_resource.unit.category.type == UnitCategory::COURSE_TYPE and unit.category.type == UnitCategory::COURSE_TYPE
      before_resource
    else
      nil
    end
  end

  def is_last_of_unit?
    !(next_resource && unit == next_resource.unit)
  end

  def skills
    exercises.map {|e| e.skills}.flatten.uniq
  end

  def skills_to_sentence
    skills.map {|s| s.to_string}.to_sentence(locale: :de)
  end
end
