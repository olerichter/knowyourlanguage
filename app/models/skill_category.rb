class SkillCategory
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  has_many :skills
end
