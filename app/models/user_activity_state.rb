class UserActivityState
  include Mongoid::Document
  include Mongoid::Timestamps

  field :resource_ids, type: Array, default: []

  belongs_to :current_resource, class_name: "Resource", optional: true
  belongs_to :user
end
