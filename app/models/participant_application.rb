class ParticipantApplication
  include Mongoid::Document

  GERMAN_KNOWLEDGE_COLLECTION = [
    'Grundkenntnisse',
    'Gute Grundkenntnisse',
    'Konversationssicher',
    'Fließend',
    'Muttersprache'
  ]

  YEARS_IN_SCHOOL_COLLECTION = [
    ['1-3 Jahre'], 
    ['4-5 Jahre'], 
    ['6-7 Jahre'], 
    ['mehr als 7 Jahre']
  ]

  ENGLISH_KNOWLEDGE_1_COLLECTION = [
    ["A1 - Einfache Situationen meistern"], 
    ["A2 - Gespräche beginnen und aufrechterhalten"], 
    ["B1 - An Diskussionen teilnehmen"], 
    ["B2 - Komplexe Situationen meistern"], 
    ["C1 - Über komplexere Themen sprechen"], 
    ["Höher"]
  ]

  ENGLISH_KNOWLEDGE_2_COLLECTION = [
    ['Ich kann mich auf einfache Art verständigen, wenn die Gesprächspartner
    langsam und deutlich sprechen.'], 
    ['Ich kann mich ohne Probleme im Ausland auf Englisch verständigen.'], 
    ['Ich kann mich ohne Probleme auf Englisch über Alltagssituationen unterhalten.'], 
    ['Ich kann praktisch alles, was ich lese oder höre, mühelos verstehen.'], 
    ['Ich lese ab und zu Texte auf Englisch.'],
    ['Ich führe persönliche oder berufliche Konversationen auf englisch.'],
    ['Ich habe bereits im Englischsprachigen Ausland gewohnt.']
  ]

  # persönliche Daten
  field :name, type: String
  field :surname, type: String
  field :date_of_birth, type: Date
  field :age, type: Integer
  field :nationality, type: String
  field :german_knowledge, type: String
  field :sex, type: String
  field :email, type: String
  field :email_confirmation, type: String
  field :phone_number, type: String

  # Sprachbiographie
  field :years_in_school, type: String
  field :english_knowledge_1, type: String
  field :english_knowledge_2, type: Array

  # Englisch-Sprachtest
  field :english_test, type: String
  field :english_test_evaluation, type: String
  

  # Termine
  field :dateset_1, type: Boolean
  field :dateset_2, type: Boolean
  field :dateset_3, type: Boolean
  field :dateset_4, type: Boolean

  # Bemerkungen
  field :comment, type: String

  validates_presence_of :name, 
            :surname, 
            :sex, 
            :nationality, 
            :german_knowledge

  validates_presence_of :english_knowledge_1, 
            :english_knowledge_2, 
            :english_test

  validates_presence_of :dateset_1, 
            :dateset_2, 
            :dateset_3,
            :dateset_4, 
            presence: true

  validates :email, confirmation: true
  validates_presence_of :email, :email_confirmation
end
