class Exercise::WordlistExercise < Exercise
  def evaluate(result)
    points = 0
    misconceptions = []
    solutions = []

    words = parsed_content.words

    result[:solutions].each do |k, v|
      next unless words[k.to_i]
      v = v.chomp.downcase.lstrip.rstrip

      if v == words[k.to_i].english.downcase
        points += 1 
        solutions << { blank: words[k.to_i].english, solution: v, correct: true }                
      else
        solutions << { blank: words[k.to_i].english, solution: v, correct: false }        
      end
    end

    { points: points, misconceptions: misconceptions, max_points: max_points, solutions: solutions }
  end

  private

  def max_points
    parsed_content.words.count
  end
end
