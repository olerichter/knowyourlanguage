class Exercise::DragDropListExercise < Exercise

  def evaluate(result)
    points = 0
    misconceptions = []
    solutions = []

    words = parsed_content.words

    result[:solutions].each do |k, v|
      if words[k.to_i].category.chomp == v.chomp
        points += 1 
        solutions << { blank: words[k.to_i].category.chomp, solution: v.chomp, correct: true }        
      else
        solutions << { blank: words[k.to_i].category.chomp, solution: v.chomp, correct: false }        
      end
    end

    { points: points, misconceptions: misconceptions, max_points: max_points, solutions: solutions }
  end

  private

  def max_points
    parsed_content.words.count
  end
end
