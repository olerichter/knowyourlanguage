class Exercise::SelfEvaluationExercise < Exercise

  MAX_POINTS = 10

  def evaluate(result)
    misconceptions = []

    points = Integer(result[:points])

    { points: points, misconceptions: misconceptions, max_points: max_points, solutions: [] }
  end

  private

  def max_points
    MAX_POINTS
  end
end
