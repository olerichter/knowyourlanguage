class Exercise::ClozeExercise < Exercise
  def evaluate(result)
    points = 0
    misconceptions = []

    solutions = []

    blanks = parsed_content.blanks

    result[:solutions].each do |k, v|
      next unless blanks[k.to_i]
      
      v = v.chomp.downcase.lstrip.rstrip
      if blanks[k.to_i].solutions.map{|b|b.chomp.downcase}.include? v
        solutions << { blank: blanks[k.to_i].solutions.first, solution: v, correct: true }
        points += 1 
      else
        solutions << { blank: blanks[k.to_i].solutions.first, solution: v, correct: false }
      end
    end

    { 
      points: points, 
      misconceptions: misconceptions, 
      max_points: max_points, 
      solutions: solutions 
    }
  end

  def max_points
    parsed_content.blanks.count
  end
end
