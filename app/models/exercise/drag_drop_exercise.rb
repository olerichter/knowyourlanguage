class Exercise::DragDropExercise < Exercise

  def evaluate(result)
    points = 0
    misconceptions = []
    solutions = []

    result[:solutions].each do |k, v|
      if parsed_content[k.to_i].answer == v.chomp
        points += 1 
        solutions << { blank: parsed_content[k.to_i].answer, solution: v.chomp, correct: true }        
      else
        solutions << { blank: parsed_content[k.to_i].answer, solution: v.chomp, correct: false }
      end
    end

    { points: points, misconceptions: misconceptions, max_points: max_points, solutions: solutions }
  end

  def all_answers
    parsed_content.each.map(&:answer)
  end

  private

  def max_points
    parsed_content.count
  end
end
