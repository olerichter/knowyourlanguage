class Skill
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Tree
  include Mongoid::Tree::Traversal

  field :name, type: String
  field :general, type: Boolean

  field :resource_urls, type: Array, default: []

  belongs_to :category, class_name: "SkillCategory"

  has_and_belongs_to_many :exercises, validate: false
  has_many :learners, class_name: "Learner::Learner"

  def to_string
    if parent
      "#{parent.to_string} › #{name}"
    else
      "#{name}"
    end
  end

  def to_string_simple
    "#{name} (#{category.name})"
  end

  def is_general?
    general
  end

  def super_traverse(&block)
    ans = []
    yield self
    children.each { |child| ans << child.super_traverse(&block) }

    { "#{name} - #{depth}" => ans }
  end

  def average
    fake_average
  end

  private

  def fake_average
    seed = Time.now.to_i/3600 + id.to_s.to_i(36)
    major = 10 * Random.new(seed).rand(4..5)

    seed = Time.now.to_i/600 + id.to_s.to_i(36)
    minor = Random.new(seed).rand(1..10)
    
    major + minor
  end
end
