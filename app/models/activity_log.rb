class ActivityLog
  include Mongoid::Document
  include Mongoid::Timestamps

  field :controller, type: String
  field :action, type: String
  field :parameter, type: String
  field :coming_from, type: String

  belongs_to :user
end
