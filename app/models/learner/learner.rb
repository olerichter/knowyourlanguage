module Learner
  class Learner
    include Mongoid::Document
    include Mongoid::Timestamps
    include Scorable

    belongs_to :user
    belongs_to :skill
    has_many :snapshots, class_name: "Learner::LearnerSnapshot", order: "created_at DESC"

    def self.general
      all.select { |l| l.skill.is_general? }
    end

    def self.in_category(category)
      all.select { |l| l.skill.category == category }
    end

    def completed_exercises
      LearnerLog.where(user: user).all.select { |l| l.exercise.skills.include?(skill) }.map(&:exercise).uniq
    end

    def repetition_score
      [(100 - (((Time.now - updated_at) / 1.hour) * 10)), 0].max
      hours_ago = (Time.now - updated_at) / 1.hour

      # [[80/Math.log(hours_ago+1) -20, 0].max, 100].min.round
      [(100 - hours_ago / (24/10)), 0].max.round
    end

    def last_repetition
      days_ago = (Time.now - updated_at) / 1.day

      days_ago < 1 ? "Heute" : "vor #{days_ago.round} Tagen"
    end
  end
end
