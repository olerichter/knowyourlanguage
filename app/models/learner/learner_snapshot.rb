module Learner
  class LearnerSnapshot
    include Mongoid::Document
    include Mongoid::Timestamps
    include Scorable

    belongs_to :learner, class_name: "Learner::Learner"

    def completed_exercises
      skill_exercises = learner.skill.exercises

      LearnerLog.where(user: learner.user, :created_at.lte => created_at).all.map(&:exercise).select { |e| skill_exercises.include?(e) }.uniq
    end

    def user
      learner.user
    end

    def skill
      learner.skill
    end
  end
end
