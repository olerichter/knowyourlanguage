module Learner
  class LearnerLog
    include Mongoid::Document
    include Mongoid::Timestamps

    field :points, type: Integer
    field :misconceptions, type: Array
    field :max_points, type: Integer

    belongs_to :user
    belongs_to :exercise

  end
end
