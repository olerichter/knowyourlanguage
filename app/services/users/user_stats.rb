module Users
  class UserStats
    def all_stats
      user_stats = full_user_stats

      {
        "Nutzer": user_stats,
        "Gruppen": group_stats(user_stats),
        "Geschlecht": gender_stats(user_stats)
      }
    end

    def group_stats user_stats
      classifiers = User::TEST_GROUPS.map do |group|
        lambda { |s| s['Gruppe'] == group }
      end
  
      cohort_stats user_stats, User::TEST_GROUPS, classifiers
    end

    def gender_stats user_stats
      group_names = ['m', 'w']
  
      classifiers = [
        lambda { |s| s['survey1_geschlecht'] == 'm' },
        lambda { |s| s['survey1_geschlecht'] == 'w' }
      ]
  
      cohort_stats user_stats, group_names, classifiers
    end

    def cohort_stats(user_stats = full_user_stats, cohort_names = User::TEST_GROUPS, cohort_classifiers = nil)
      cohorts = []

      cohort_names.each_with_index do |cohort_name, index|
        cohort = {}

        if cohort_classifiers
          cohort_user_stats = user_stats.select &cohort_classifiers[index]
        else
          cohort_user_stats = user_stats.select { |s| s['cohort'] == cohort_name }
        end

        cohort_field cohort_user_stats, 'Übungen absolviert'

        cohort['cohort'] = cohort_name
        cohort['count'] = cohort_user_stats.count
        cohort['Übungen absolviert'] = cohort_field cohort_user_stats, 'Übungen absolviert'
        cohort['Verschiedene Übungen absolviert'] = cohort_field cohort_user_stats, 'Verschiedene Übungen absolviert'
        cohort['Durchschnittliche Punktzahl'] = cohort_field cohort_user_stats, 'Durchschnittliche Punktzahl'
        cohort['Maximale Punktzahl (nur Kompetenzkategorien)'] = cohort_field cohort_user_stats, 'Maximale Punktzahl (nur Kompetenzkategorien)'
        cohort['Minimale Punktzahl (nur Kompetenzkategorien)'] = cohort_field cohort_user_stats, 'Minimale Punktzahl (nur Kompetenzkategorien)'
        cohort['Bandbreite der Punktzahl'] = cohort_field cohort_user_stats, 'Bandbreite der Punktzahl'
        cohort['Übungen geöffnet vom Feedback'] = cohort_field cohort_user_stats, 'Übungen geöffnet vom Feedback'
        cohort['Übungen geöffnet vom Visuellen Feedback'] = cohort_field cohort_user_stats, 'Übungen geöffnet vom Visuellen Feedback'
        cohort['Übungen geöffnet vom Text-Feedback'] = cohort_field cohort_user_stats, 'Übungen geöffnet vom Text-Feedback'

        # round stats
        cohort.each { |key, value| cohort[key] = value.round(2) if value.is_a? Numeric }

        cohorts << cohort
      end
      
      cohorts
    end

    def cohort_field group_user_stats, field_name
      group_user_stats.map{ |s| s[field_name]}.inject{ |sum, el| sum + el }.to_f / group_user_stats.size
    end

    def full_user_stats
      user_list = [
        'nutzer1@knowyourlanguage.de',
        'nutzer2@knowyourlanguage.de',
        'nutzer3@knowyourlanguage.de',
        'nutzer4@knowyourlanguage.de',
        'nutzer5@knowyourlanguage.de',
        'nutzer6@knowyourlanguage.de',
        'nutzer7@knowyourlanguage.de',
        'nutzer8@knowyourlanguage.de',
        'nutzer9@knowyourlanguage.de',
        'nutzer10@knowyourlanguage.de',
        'nutzer11@knowyourlanguage.de',
        'nutzer12@knowyourlanguage.de',
        'nutzer13@knowyourlanguage.de',
        'nutzer14@knowyourlanguage.de',
        'nutzer15@knowyourlanguage.de'
      ]
      
      users = []

      User.all.order_by(:test_group.asc).select{|u| user_list.include? u.email}.each do |user|
        max_root_points_field = max_root_points(user)
        min_root_points_field = min_root_points(user)
        user_stat = {
                   'Email' => user.email,
                   'Gruppe' => user.test_group,
                   'Übungen absolviert' => exercise_count(user),
                   'Verschiedene Übungen absolviert' => distinct_exercise_count(user),
                   'Durchschnittliche Punktzahl' => average_points(user),
                   'Maximale Punktzahl (nur Kompetenzkategorien)' => max_root_points_field,
                   'Minimale Punktzahl (nur Kompetenzkategorien)' => min_root_points_field,
                   'Übungen geöffnet vom Feedback' => opened_exercise_from_feedback(user),                   
                   'Übungen geöffnet vom Visuellen Feedback' => opened_exercise_from_visual_feedback(user),
                   'Übungen geöffnet vom Text-Feedback' => opened_exercise_from_text_feedback(user)
                  }

        user_stat['Bandbreite der Punktzahl'] = max_root_points_field && min_root_points_field ? max_root_points_field - min_root_points_field : -1
        
        # get survey attributes
        user.attributes.each do |attribute|
          if attribute.first && attribute.first.start_with?('survey1_')
            user_stat[attribute.first] = attribute.second 
          end
        end
        
        users << user_stat             
      end

      users
    end

    def exercise_count(user)
      learner_log_for(user).count.round(2)
    end
  
    def distinct_exercise_count(user)
      learner_log_for(user).map { |l| l.exercise }.uniq.count.round(2)
    end
  
    def average_points(user)
      points = learner_for(user).select{ |l| l.skill.leaf?}.map {|l| l.points}
  
      (1.0 * points.inject{ |sum, el| sum + el }.to_f / points.size).round(2)
    end

    def max_root_points(user)
      learner_for(user).select{ |l| l.skill.root?}.map {|l| l.points}.max.round(2)
    end

    def min_root_points(user)
      learner_for(user).select{ |l| l.skill.root?}.map {|l| l.points}.min.round(2)
    end
  
    def opened_exercise_from_feedback(user)
      log_for(user).select{|l| l.controller == 'skills'}.count.round(2)
    end

    def opened_exercise_from_visual_feedback(user)
      log_for(user).select{|l| l.controller == 'skills' && eval(l.parameter)['from'].try(:include?, 'visual_feedback') }.count.round(2)
    end

    def opened_exercise_from_text_feedback(user)
      log_for(user).select{|l| l.controller == 'skills' && eval(l.parameter)['from'].try(:include?, 'text_feedback') }.count.round(2)
    end

    private
      
    def log_for(user, end_date = nil)
      if end_date
        ActivityLog.where(user: user, :created_at.lte => end_date).all
      else
        ActivityLog.where(user: user).all
      end
    end

    def learner_for(user, end_date = nil)
      if end_date
        Learner::LearnerSnapshot.where(user: user, :created_at.lte => end_date).order_by(:created_at.desc).first
      else
        Learner::Learner.where(user: user).all
      end
    end
  
    def learner_log_for(user, end_date = nil)
      if end_date
        Learner::LearnerLog.where(user: user, :created_at.lte => end_date).all
      else
        Learner::LearnerLog.where(user: user).all
      end
    end
  end
end
