module Exercises
  class EvaluationService
    def self.evaluate(params)
      new(params).evaluate
    end

    def initialize(params)
      @exercise = params[:exercise]
      @user = params[:user]
      @evaluation = params[:evaluation_params]

      @evaluation.delete :solutions
    end

    def evaluate
      @learner_log = Learner::LearnerLog.new @evaluation
      @learner_log.user = @user
      @learner_log.exercise = @exercise
      @learner_log.save!

      @exercise.skills.each do |skill|
        Learners::UpdateService.update @evaluation.merge(user: @user, skill: skill, exercise_id: @exercise.id)
      end
    end

  end
end
