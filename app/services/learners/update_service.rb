module Learners
  class UpdateService
    MOVING_AVERAGE_FACTOR = 0.7

    def self.update(params)
      new(params).update
    end

    def initialize(params)
      @params = params
      @learner = Learner::Learner.where(user: @params[:user], skill: @params[:skill]).first
    end

    def update
    update_learner @params[:skill]
    update_parents @params[:skill]
    end

    private

    def update_parents skill
      if skill.parent
        update_learner_as_parent skill.parent
        update_parents skill.parent
      end     
    end

    def update_learner skill
      @learner = Learner::Learner.where(user: @params[:user], skill: @params[:skill]).first

      if @learner
        @learner.points = [(1 - time_factor) * @learner.points + time_factor * calculated_points, 1].max
        @learner.progress += 1
        @learner.save!
      else
        @learner = Learner::Learner.new user: @params[:user],
                                        skill: skill,
                                        points: calculated_points,
                                        progress: 1
        @learner.save!

        skill.learners << @learner
        skill.save!
      end

      create_learner_snapshot @learner
    end

    def update_learner_as_parent skill
      learner = Learner::Learner.where(user: @params[:user], skill: skill).first
      unless learner
        learner = Learner::Learner.create!(user: @params[:user], skill: skill, progress: 0)
      end

      children_points = []    

      children_learner = skill.children.map {|c| Learner::Learner.where(user: @params[:user], skill: c).first}
      children_learner.each do |learner|
        children_points << learner.points if learner
      end

      if children_points
        learner.points = children_points.reduce(:+) / children_points.size.to_f
        learner.progress += 1
        learner.save!
      end
    
      create_learner_snapshot learner
    end

    def create_learner_snapshot learner
      snapshot = Learner::LearnerSnapshot.new points: learner.points,
                                              progress: learner.progress
      learner.snapshots << snapshot
      learner.save!
    end

    def calculated_points
      100.0 * @params[:points] / @params[:max_points]
    end

    def time_factor
      last_activity = @learner.updated_at
      timediff = ((Time.now - last_activity) / 1.minutes)

      [[0.004*timediff**2 + 0.4, 0].max, 1].min
    end
  end
end
