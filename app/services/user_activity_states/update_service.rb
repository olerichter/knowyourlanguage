module UserActivityStates
  class UpdateService
    def self.update(params)
      new(params).update
    end

    def initialize(params)
      @params = params

      fetch_or_create_state
    end

    def update
      update_resources if @params[:finished_resource]
      update_current_resource if @params[:current_resource]
    end

    private

    def update_resources
      resource = Resource.find @params[:finished_resource]


      if resource.exercises.select {|e| Learner::LearnerLog.where(user: @params[:user], exercise: e).empty?}.empty?
        @state.resource_ids << resource.id unless @state.resource_ids.include?(resource.id)
        @state.save!
      end
    end

    def update_current_resource
      resource = Resource.find @params[:current_resource]

      @state.current_resource = resource
      @state.save!
    end

    def fetch_or_create_state
      @state = UserActivityState.find_or_create_by(user: @params[:user])
    end
  end
end
