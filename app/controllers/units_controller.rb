class UnitsController < ApplicationController
  before_filter :authenticate_user!
  before_action :fetch_categories, only: [:index]
  before_action :fetch_unit, only: [:show]

  def fetch_categories
    @categories = UnitCategory.all
    
  end

  def fetch_unit
    @unit = Unit.find params[:id]
  end
end
