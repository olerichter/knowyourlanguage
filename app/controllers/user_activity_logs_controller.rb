class UserActivityLogsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @log = UserActivityLog.create log_params.merge(user: current_user)

    render nothing: true
  end

  private

  def log_params
    params
      .require(:user_activity_log)
      .permit!
  end
end
