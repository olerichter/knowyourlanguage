class LearnerLogsController < ApplicationController
  before_filter :authenticate_user!
  before_action :fetch_learner_log, only: [:destroy]

  def destroy
    @log_entry.destroy

    redirect_to feedback_path
  end

  private

  def fetch_learner_log
    @log_entry = Learner::LearnerLog.find params[:id]
  end
end
