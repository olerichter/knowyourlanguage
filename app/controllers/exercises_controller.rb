
class ExercisesController < ApplicationController
  before_filter :authenticate_user!

  respond_to :js, only: [:evaluate]
  before_action :fetch_exercise, only: [:show, :evaluate]
  before_action :fetch_exercises, only: [:index]

  def evaluate
    @evaluation = @exercise.evaluate evaluation_params

    params = {
      exercise: @exercise,
      user: current_user,
      evaluation_params: @evaluation.clone
    }

    Exercises::EvaluationService.evaluate(params)
  end

  def show
  end

  private

  def fetch_exercise
    if hexadecimal? params[:id].to_s
      @exercise = Exercise.find params[:id]
    else
      @exercise = Exercise.find_by! wp_id: params[:id]
    end
  end

  def fetch_exercises
    @exercises = Exercise.all    
  end

  def evaluation_params
    params
      .require(:exercise)
      .permit!
  end

  def hexadecimal?(string)
    # /[0-9a-f]{24}/ =~ string
    string.length == 24
  end
end
