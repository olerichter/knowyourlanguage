class TestGroupPagesController < ApplicationController
  before_filter :authenticate_user!

  before_action :fetch_learner, only: [:show]
  before_action :fetch_exercises, only: [:show]
  before_action :fetch_skills, only: [:show]

  def index

  end

  def show
    if valid_page?
        render template: "test_group_pages/#{params[:group]}"
    else
      render file: "public/404.html", status: :not_found
    end
  end

  private
  
  def fetch_learner
    @learner = Learner::Learner.where(user_id: current_user.id).includes(:skill).includes(:exercises, from: :skill).index_by(&:id)
  end

  def fetch_exercises
    @exercises = Exercise.all.includes(:resources)
  end

  def fetch_skills
    @skills = Skill.includes(:children).includes(:learners).includes(:exercises).includes(:resources, from: :exercises).all.index_by(&:id)
  end
  
  def valid_page?
    File.exist?(Pathname.new(Rails.root + "app/views/test_group_pages/#{params[:group]}.html.erb"))
  end

end
