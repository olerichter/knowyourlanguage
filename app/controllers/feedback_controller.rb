class FeedbackController < ApplicationController
  before_filter :authenticate_user!
  before_action :fetch_learner, only: [:index]
  before_action :fetch_learner_logs, only: [:index]
  before_action :fetch_exercises, only: [:index]
  before_action :fetch_skills, only: [:index]
  
  # def index
  #   # log ActivityLog::OPEN_FEEDBACK, type: params[:feedback]
  # end

  private

  def fetch_learner
    @learner = Learner::Learner.where(user_id: current_user.id).includes(:skill)
  end

  def fetch_learner_logs
    @learner_logs = Learner::LearnerLog.where(user_id: current_user.id).order_by(created_at: 'desc')
  end

  def fetch_skills
    @skills = Skill.includes(:children).includes(:exercises).includes(:resources, from: :exercises).all.index_by(&:id)
  end

  def fetch_exercises
    @exercises = Exercise.all.includes(:resources).index_by(&:id)
  end

  # def test
  #   output = []
    
  #   Exercise.includes(:skills, :resource).all.each do |exercise|
  #     if exercise.skills.empty?

  #       e = {
  #         "id" => exercise.wp_id.to_s,
  #         "name" => exercise.resource.try(:name),
  #         "skills" => [{category: nil, topic: nil}],
  #         "level" => "4",
  #         "niveau" => "A1"
  #       }
        
  #       output << e
  #     end
  #   end
  #   puts output.to_yaml
  # end
end
