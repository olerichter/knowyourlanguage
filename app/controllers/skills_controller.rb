class SkillsController < ApplicationController
  rescue_from Mongoid::Errors::DocumentNotFound, with: :not_found
  
  before_filter :authenticate_user!
  before_action :fetch_skill, only: [:practice]

  def practice
    UserActivityLog.create!(user: current_user, event_type: UserActivityLog::OPEN_EXERCISE_FROM_FEEDBACK_EVENT, detail: @skill.name)
  end

  private

  def fetch_skill
    @skill = Skill.find(params[:id])
  end
end
