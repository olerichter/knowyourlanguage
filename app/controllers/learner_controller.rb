class LearnerController < ApplicationController
  before_filter :authenticate_user!
  before_action :fetch_learner, only: [:update]

  def update
    params = {
      user: @learner.user,
      skill: @learner.skill,
      points: update_params[:points].to_i,
      max_points: update_params[:max_points].to_i
    }
    
    Learners::UpdateService.update params

    redirect_to :back
  end

  private

  def fetch_learner
    @learner = Learner::Learner.find params[:id]
  end

  def update_params
    params
    .require(:learner_learner)
    .permit!
  end
end
