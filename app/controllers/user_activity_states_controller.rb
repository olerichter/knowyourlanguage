class UserActivityStatesController < ApplicationController
  before_filter :authenticate_user!

  def create
    @state = UserActivityState.find_or_create_by(user: current_user)

    UserActivityStates::UpdateService.update user: current_user, finished_resource: state_params[:resource_id]

    render nothing: true
  end

  private

  def state_params
    params
      .require(:user_activity_state)
      .permit!
  end
end
