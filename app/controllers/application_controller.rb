class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :log, if: -> { current_user }

  private

  def log()
    parameter = params
    user_id = current_user.id
    controller = parameter.delete "controller"
    action = parameter.delete "action"
    parameter = parameter.to_s

    ActivityLog.create!(
      user_id: current_user.id,
      controller: controller,
      action: action,
      parameter: parameter
    )
  end

  def authenticate_admin!
    redirect_to root_path unless current_user.is_admin? 
  end

end
