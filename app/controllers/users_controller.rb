class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_admin!
  before_action :fetch_users, only: :index
  before_action :fetch_users, only: :index
  before_action :fetch_user, only: [:edit, :update]
  before_action :fetch_stats, only: :index


  def index
  end

  def edit
    
  end

  def update    
    respond_to do |format|
      if @user.update(post_params)
        format.html { redirect_to edit_user_path(@user), notice: 'user was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def new
    @user = User.new
  end

  def create    
    respond_to do |format|
      if @user.update(post_params)
        format.html { redirect_to edit_user_path(@user), notice: 'user was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  private

  def fetch_stats
    @stats = Users::UserStats.new.all_stats
  end

  def post_params
    params.require(:user).permit!
  end

  def fetch_users
    @users = User.order_by(:email.asc).all
  end

  def fetch_user
    @user = User.find(params[:id])
  end
end
