class LearnController < ApplicationController
  before_filter :authenticate_user!

  def index
    if is_user_in_test_group
      if current_user.test_group == 'no_feedback_feedback'
        redirect_to test_group_page_path('no_feedback')

      else
        redirect_to test_group_page_path(current_user.test_group)
      end
    else
      redirect_to test_group_pages_path
    end
  end

  def unit_topics
    course_units = UnitCategory.where(type: 'course').map {|c| c.units}.flatten
    course_skills = course_units.map {|u| u.skills}.flatten.uniq
    
    exercise_units = UnitCategory.where(type: 'exercise').map {|c| c.units}.flatten
    exercise_skills = exercise_units.map {|u| u.skills}.flatten.uniq

    not_relevant_exercises = exercise_skills.select {|e| !course_skills.include?(e)}
  end

  private

  def is_user_in_test_group
    not current_user.test_group && current_user.test_group.empty?
  end
end
