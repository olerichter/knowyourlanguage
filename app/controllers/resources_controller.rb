class ResourcesController < ApplicationController
  rescue_from Mongoid::Errors::DocumentNotFound, with: :not_found
  
  before_filter :authenticate_user!

  def show
    @resource = Resource.find params[:id]

    beautify_content

    UserActivityStates::UpdateService.update user: current_user, current_resource: @resource.id
  end

  private

  def beautify_content
    content = Nokogiri::HTML(@resource.content)
    content.css('.slideshow').remove
    content.css('.clear').remove
    content.css('.well').remove_class 'well'
    content.css('.article-finish-contents').remove

    # content.css('.element-wordlist').each do |w|
    #   w.css('table').each do |t|
    #     t.attributes['class'].value = "table table-striped"
    #   end

    #   w.css('.wordlist-header').each do |h|
    #     h.remove
    #   end

    #   w.css('.content').each do |h|
    #     h.children = h.css('li').first.content
    #   end
    # end

    @resource.content = content.to_html
  end

  def not_found
    redirect_to root_url, alert: "resource not found."
  end
end
