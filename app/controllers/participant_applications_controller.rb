class ParticipantApplicationsController < ApplicationController
  before_action :set_participant_application, only: [:show, :edit, :update, :destroy, :confirmation]
  before_action :fetch_exercise, only: [:new, :create, :update, :edit]
  before_filter :auth_user, except: [:new, :create, :confirmation]
  before_filter :authenticate_admin!, except: [:new, :create, :confirmation]

  def index
    @participant_applications = ParticipantApplication.order_by(:english_knowledge_1.asc).all
  end

  def show
    redirect_to action: :edit
  end

  def confirmation
    render layout: 'participant_applications'
  end

  def new
    @participant_application = ParticipantApplication.new

    render layout: 'participant_applications'
  end

  def edit
  end

  def create    
    create_params = participant_application_params

    create_params[:english_test_evaluation] = @exercise.evaluate(create_params[:exercise]).to_json
    create_params[:english_test] = create_params.delete(:exercise).to_json

    @participant_application = ParticipantApplication.new(create_params)

    respond_to do |format|
      if @participant_application.save
        # format.html { redirect_to @participant_application, notice: 'Participant application was successfully created.' }
        format.html { redirect_to confirmation_participant_application_path(@participant_application) }
      else
        format.html { render :new, message: "Bitte füllen Sie alle Felder aus." }
      end
    end

  rescue
    redirect_to new_participant_application_path
  end

  def update
    create_params = participant_application_params

    create_params[:english_test_evaluation] = @exercise.evaluate(create_params[:exercise]).to_json
    create_params[:english_test] = create_params.delete(:exercise).to_json
    
    respond_to do |format|
      if @participant_application.update(create_params)
        format.html { redirect_to @participant_application, notice: 'Participant application was successfully updated.' }
        format.json { render :show, status: :ok, location: @participant_application }
      else
        format.html { render :edit }
        format.json { render json: @participant_application.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @participant_application.destroy
    respond_to do |format|
      format.html { redirect_to participant_applications_url, notice: 'Participant application was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def fetch_exercise
      @exercise = Exercise.where(wp_id: 'application_exercise').first
    end

    def set_participant_application
      @participant_application = ParticipantApplication.find(params[:id])
    end

    def participant_application_params
      params.require(:participant_application).permit!
    end

    def parse_date_of_birth
      new_params = participant_application_params
      params_date = ""
      params_date += new_params.delete 'date_of_birth(3i)'
      params_date += '.'
      params_date += new_params.delete 'date_of_birth(2i)'
      params_date += '.'
      params_date += new_params.delete 'date_of_birth(1i)'
      new_params
    end

    def auth_user
      redirect_to new_participant_application_path unless user_signed_in?
    end
end
