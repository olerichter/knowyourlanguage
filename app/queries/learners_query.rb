class LearnersQuery
  def self.history_steady(learner, step, first_date, last_date)
    new(learner).history_steady(step, first_date, last_date)
  end

  def self.enumerate_dates(start, end_, step)
    new(nil).enumerate_dates(start, end_, step)
  end

  def initialize(learner)
    @learner = learner
  end

  def history_steady(step, first_date, last_date)
    history = []
    snapshots = @learner.snapshots.order_by(:created_at.asc)

    enumerate_dates(first_date, last_date, step).each do |s|
      last_snapshot = snapshots.where(:created_at.lte => s).order_by(:created_at.asc).last
      history << { date: s, snapshot: last_snapshot }
    end

    history
  end

  def enumerate_dates(start, end_, step)
    Enumerator.new { |y| loop { y.yield start; start += step } }.take_while { |d| d < end_ }
  end
end
