
require 'YAML'
require 'JSON'
require 'byebug'






################# MAIN ########################

exercises = []
resources = []

files = Dir["additional_content/*_resources.yml"].each do |file|
  resources += YAML.load_file(file)
end

resources.each do |resource|
  resource["exercises"].each do |exercise| 
    exercise["content"] = exercise["content"].to_json 
    exercise["id"] = "#{resource["name"]}_#{exercise["id"]}"
  end

  exercises += resource["exercises"]  
  resource["exercises"] = resource["exercises"].map{|e| e["id"]}

  puts '.'
end

existing_exercises = JSON.parse(File.read('final_exercises.json'), symbolize_names: true)
existing_resources = JSON.parse(File.read('final_resources.json'), symbolize_names: true)

exercises.each do |exercise|
  existing_exercises << exercise unless existing_exercises.any? {|e| e[:id] == exercise["id"]}
end

resources.each do |resource|  
  existing_resources << resource unless existing_resources.any? {|r| r[:id] == resource['id']}
end

File.write('final_exercises.json', JSON.pretty_generate(existing_exercises))
File.write('final_resources.json', JSON.pretty_generate(existing_resources))
