---
id: '18984411753721570000011342499013'
content: '<div><div class="entry-content"><div class="browser-support"><!-- noscript
  Meldung --><noscript><div class="noscript-info"><p>Bitte aktiviere Javascript, um
  den vollen Funktionsumfang des Portals nutzen zu können.</p></div></noscript><!--
  IE Version Meldung --> <!--[if lte IE 7]><div class="ieversion-info"><p>Du scheinst
  eine alte Version des Internet Exlorers zu verwenden. Das Portal erfordert den Internet
  Explorer in der Version 6.0 oder höher.</p></div><![endif]--></div><div lang="en"
  xml:lang="en"><div class="module-output element-text" id="text-3581247600328"><div
  class="module-output content-snippet" id="contentsnippet-58872623514320501441230078"><div
  class="tab-content well"><div lang="de" xml:lang="de"><h1>Fragen in der Zeitform
  Simple Present</h1><h2>Entscheidungsfragen</h2><div class="paragraph">Ja-Nein-Fragen
  fragen jemanden nach einer Entscheidung. Die Antwort kann Ja oder Nein sein.</div><div
  class="block-markup rule "><div class="paragraph">Um Fragen im Simple Present zu
  formulieren, benutze <span class="use">"do"</span> oder <span class="use">"does"</span>
  + Hauptverb:<ul><li>Gebrauche <span class="use">"do"</span> mit den <span class="grammar">Pronomen</span>
  I, you, we, they.</li><li>Gebrauche <span class="use">"does"</span> mit den Pronomen
  he, she, it.</li></ul></div><div class="paragraph"><ul><li><span class="ex-sentence">I
  like parties.</span> – <span class="ex-sentence"><strong>Do</strong> you like parties?</span></li><li><span
  class="ex-sentence">You eat ice cream.</span> – <span class="ex-sentence"><strong>Do</strong>
  you eat ice cream?</span></li><li><span class="ex-sentence">He like<strong>s</strong>
  music.</span> – <span class="ex-sentence"><strong>Does</strong> he like music?</span></li><li><span
  class="ex-sentence">She play<strong>s</strong> the drums.</span> – <span class="ex-sentence"><strong>Does</strong>
  she play the drums?</span></li><li><span class="ex-sentence">It work<strong>s</strong>
  well.</span> – <span class="ex-sentence"><strong>Does</strong> it work well?</span></li><li><span
  class="ex-sentence">We watch TV.</span> – <span class="ex-sentence"><strong>Do</strong>
  we watch TV?</span></li><li><span class="ex-sentence">They like football.</span>
  – <span class="ex-sentence"><strong>Do</strong> they like football?</span></li></ul></div><div
  class="paragraph">Beachte: Benutze <strong>do</strong> oder <strong>does</strong>
  + die Grundform des Hauptverbs! Füge kein <strong>s</strong> an das Verb an: <strong>Does</strong>
  he play<strong>s</strong> the drums? —> Das ist falsch!Das <strong>s</strong> von
  he, she, ist bereits in der Form <strong>does</strong> enthalten!</div></div><div
  class="block-markup prompt "><div class="paragraph">Beginnen die Sätze mit <strong>Do</strong>
  oder <strong>Does</strong>?</div></div><!--output-specification-for-prompt--></div></div><!--output-specification-for-multilang-with-prompt--></div><div
  class="clear"> </div><div class="module-output content-snippet" id="contentsnippet-58872623514320601489596726"><div
  lang="en" xml:lang="en">%s<div class="clear"> </div></div></div><div class="clear"> </div><div
  class="module-output content-snippet" id="contentsnippet-58872623514321651459513432"><div
  class="tab-content well"><div lang="de" xml:lang="de"><div class="block-markup prompt
  "><div class="paragraph">Schreibe Fragen in der Zeitform Simple Present. Benutze
  die Wörter aus den Antworten. Zum Beispiel: Wenn die Antwort <span class="ex-sentence">Yes,
  she plays football.</span> ist, dann schreibe <span class="ex-sentence">Does she
  play football?</span>.</div></div><!--output-specification-for-prompt--></div></div><!--output-specification-for-multilang-with-prompt--></div><div
  class="clear"> </div><div class="module-output content-snippet" id="contentsnippet-5887262351432166203706606"><div
  lang="en" xml:lang="en">%s<div class="clear"> </div></div></div><div class="clear"> </div><div
  class="module-output content-snippet" id="contentsnippet-58872623514320511881088212"><div
  class="tab-content well"><div lang="de" xml:lang="de"><h2>Kurzantworten</h2><div
  class="paragraph">Erinnere dich daran, wie man Entscheidungsfragen mit dem Verb
  <span class="use">"to be"</span> bildet: <span class="ex-sentence">Are you tired?</span>.
  Du hast bereits gelernt, dass man darauf nicht einfach mit Ja oder Nein antwortet,
  da dies manchmal unhöflich ist. Stattdessen benutzt du eine Kurzantwort: <span class="ex-sentence">Yes,
  I am.</span> oder <span class="ex-sentence">No, I am not.</span>.</div><div class="paragraph">Die
  gleiche Regel gilt auch für Kurzantworten auf Entscheidungsfragen mit anderen Hauptverben:</div><div
  class="block-markup rule "><div class="paragraph"><ul><li><span class="ex-sentence"><strong>Do</strong>
  you go to the party?</span> – <span class="ex-sentence">Yes, I do.</span> or <span
  class="ex-sentence">No, I don''t.</span></li><li><span class="ex-sentence"><strong>Does</strong>
  he like music?</span> – <span class="ex-sentence">Yes, he does.</span> or <span
  class="ex-sentence">No, he doesn''t.</span></li><li><span class="ex-sentence"><strong>Does</strong>
  she play the drums?</span> – <span class="ex-sentence">Yes, she does.</span> or
  <span class="ex-sentence">No, she doesn''t.</span></li><li><span class="ex-sentence"><strong>Does</strong>
  it work well?</span> – <span class="ex-sentence">Yes, it does.</span> or <span class="ex-sentence">No,
  it doesn''t.</span></li><li><span class="ex-sentence"><strong>Do</strong> we drive
  to the city?</span> – <span class="ex-sentence">Yes, we do.</span> or <span class="ex-sentence">No,
  we don''t.</span></li><li><span class="ex-sentence"><strong>Do</strong> they like
  football?</span> – <span class="ex-sentence">Yes, they do.</span> or <span class="ex-sentence">No,
  they don''t.</span></li></ul></div><div class="paragraph">Yes/No + Personalpronomen
  + do/don''t oder does/doesn''t</div><div class="paragraph">Noch einmal:<ul><li>Gebrauche
  <strong>do</strong> mit I, you, we, they.</li><li>Gebrauche <strong>does</strong>
  mit he, she, it.</li></ul></div></div></div></div><!--output-specification-for-multilang-without-prompt--><div
  class="a-content-wrapper" style="text-align:center;"><ins class="adsbygoogle" style="display:inline-block;max-width:612px;width:100%;"
  data-ad-client="ca-pub-0506161449776710" data-ad-slot="4335526269" data-ad-type="text,staticimage"
  data-ad-format="horizontal"></ins></div></div><div class="clear"> </div><div class="module-output
  content-snippet" id="contentsnippet-5887262351435989959611994"><div class="tab-content
  well"><div lang="de" xml:lang="de"><div class="block-markup prompt "><div class="paragraph">Lies
  den folgenden Text und finde die Fehler. Wähle die falschen Formen von <strong>do</strong>
  oder <strong>does</strong> aus, indem du sie anklickst.</div></div><!--output-specification-for-prompt--></div></div><!--output-specification-for-multilang-with-prompt--></div><div
  class="clear"> </div><div class="module-output content-snippet" id="contentsnippet-5887262351432061779653135"><div
  lang="en" xml:lang="en">%s<div class="clear"> </div></div></div><div class="clear"> </div><div
  class="clear"> </div><div class="tab-content well"><div lang="de" xml:lang="de"><div
  class="paragraph">Du hast den Grammatikteil dieser Unit beendet. Gut gemacht!</div></div></div><!--output-specification-for-multilang-without-prompt--><div
  class="link-wrapper clearfix"></div><div class="clear"> </div></div><div class="clear"> </div></div></div></div>'
exercises:
- '36222737976648620000011129037838'
- '34522722484265810000012036802915'
- '18984411753721570000011342499013'
name: 'Grammatik: Fragen mit do/does'
unit_name: 'Kurs - Unit 6: Meeting the neighbours'
type: 
