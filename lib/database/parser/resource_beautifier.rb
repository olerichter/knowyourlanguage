require 'nokogiri'
require 'JSON'
require 'open-uri'
require 'pp'
require 'byebug'
require 'YAML'
require 'cgi'

class ResourceBeautifier
  def self.beautify(content)
    new(content).beautify
  end

  def initialize(content)
    @content = content
  end

  def beautify
    beautify_wordlists    
    bueatity_miscellaneous
    beautify_multilang_tabs

    beautify_finalizer

    @content
  end

  def bueatity_miscellaneous
    content = Nokogiri::HTML(@content)

    # audio
    content.css('.audio-wrapper-wrapper').each do |wrapper|
      mp3_url = wrapper.css('source').attr('src').value

      byebug if mp3_url.empty?

      audio_control = "<audio controls><source src='#{mp3_url}' type='audio/mpeg'>Your browser does not support the audio element.</audio>"
      wrapper.replace audio_control
    end

    # sample field
    content.css('.element-sample-solution-field').each do |container|
      words = container.content
      toggle = "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#demo'>Zeige Wörter</button><div id='demo' class='collapse'>#{words}</div>"
      container.replace toggle
    end

    # remove language
    # ToDo

    # filter stuff
    content.css('a').each do |link|
      if link.parent.name == 'li' && link.parent.parent.parent.attr('class') == 'paragraph'
        link.remove_attribute 'href'
        link.name = 'em'
      end
    end

    # remove classes and tags
    tag_names = %w(.buchstabenraten-quiz .abuser-content a script)
    tag_names.each do |tag_name|
      content.css(tag_name.to_s).remove
    end

    # tables
    content.css('.text-table-wrapper').each do |table|
      table.css('table').each do |t|
        t.attributes['class'].value = "table table-striped"
      end

      table.css('th').each do |th|
        th.remove_attribute('style')
        th.remove_attribute('class')
      end
  
      table.css('td').each do |td|
        td.remove_attribute('style')
      end  

      table.css('.col-1').each do |col1|
        col1.children = "<b>#{col1.content}</b>"
      end

      table.css('.clear').each do |c|
        c.remove
      end               
    end    

    content.css('.element-wordlist').each do |w|
      w.css('table').each do |t|
        t.attributes['class'].value = "table table-striped"
      end

      w.css('.wordlist-header').each do |h|
        h.remove
      end

      w.css('.content').each do |h|
        h.children = h.css('li').first.content
      end
    end

    @content = content.to_html
  end

  def beautify_finalizer
    content = Nokogiri::HTML(@content)

    content = content.at_css('body')
    content.name = 'div'

    @content = content.to_html
  end

  def beautify_wordlists
    content = Nokogiri::HTML(@content)
    content.css('.wordlist').each do |wordlist|
      wordlist_html = "<div class='panel panel-default'><table class='table'>
                        <thead>
                          <tr>
                            <th>Deutsch</th>
                            <th>Übersetzung</th>
                            <th>Definition</th>
                            <th>Beispiel</th>
                          </tr>
                        </thead>
                        <tbody>"

      wordlist.css('.wordlist-item').each do |item|
        word = item.css('.word').first.content
        definition = item.css('.wordlist-definition .content-wrapper .content').first.to_html if item.css('.wordlist-definition .content-wrapper .content').first
        translation = item.css('.wordlist-translation .content-wrapper .content').first.to_html if item.css('.wordlist-translation .content-wrapper .content').first
        example = item.css('.wordlist-example .content-wrapper .content').first.to_html if item.css('.wordlist-example .content-wrapper .content').first

        wordlist_html += "<tr><td><b>#{word}</b></td>
                        <td>#{translation}</td>
                        <td>#{definition}</td>
                        <td>#{example}</td>
                          </tr></div>"
      end

      wordlist_html += "</tbody>
              </table>"

      wordlist.replace Nokogiri::HTML(wordlist_html).root
    end

    @content = content.to_html
  end

  def beautify_multilang_tabs
    content = Nokogiri::HTML(@content)

    # multilang tabs
    content.css('.element-multi-lang').each do |element_multi_lang|
      id = element_multi_lang['id']

      element_multi_lang['class'] = 'tabbable'
      element_multi_lang.css('ul').each do |i|
        i['class'] = 'nav nav-tabs' unless i.parent['class'] == 'paragraph'
      end

      element_multi_lang.css('li')[0]['class'] = 'active'
      element_multi_lang.css('li')[0].add_child "<a href='#en-#{id}' data-toggle='tab'>English</a>"

      element_multi_lang.css('li')[1].add_child "<a href='#de-#{id}' data-toggle='tab'>Deutsch</a>"

      # en_content = element_multi_lang.css('.textile').first
      # en_content['class'] = 'tab-pane active'
      # en_content['id'] = "en-#{id}"
      # byebug
      de_content = element_multi_lang.css("div[lang='de']")[0]
      # de_content['class'] = 'tab-pane'
      # de_content['id'] = "de-#{id}"

      # delete if content is empty
      if de_content.css('.paragraph').first && de_content.css('.paragraph').first.content.strip.empty?
        element_multi_lang.remove
        next
      end

      content_container = Nokogiri::XML::Node.new 'div', content
      content_container[:class] = 'tab-content well'
      de_content.parent = content_container
      element_multi_lang.replace content_container

      
      # de_content.parent = content_container

      # element_multi_lang.add_next_sibling content_container

      # content_container.add_child content_container
      # en_content.parent = content_container
      # de_content.parent = content_container

      # tables
      content.css('.text-table-wrapper').each do |table|
        table.css('th').each do |th|
          th.remove_attribute('style')
        end

        table.css('tr').each do |td|
          td.remove_attribute('style')
        end                  
      end
        

    end

    @content = content.to_html
  end
end

################### MAIN ###################

resources = JSON.parse(File.read('final_resources_pure.json'), symbolize_names: true)

resources.each do |resource|
  resource[:content] = ResourceBeautifier.beautify resource[:content]
  puts '.'
end

File.write('final_resources.json', JSON.pretty_generate(resources))
