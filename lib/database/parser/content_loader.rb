require 'fileutils'
require 'JSON'
require 'YAML'
require 'byebug'

def slugify name
  name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
end

exercise_path = 'content/exercises'
resource_path = 'content/resources'

resources_test = []
exercises_test = []

Dir.entries("#{resource_path}").sort.each do |entry|
  files = Dir["#{resource_path}/#{entry}/*.yml"].sort.each do |file|
    begin
      resources_test << YAML.load_file(file)
    rescue
      byebug
    end
  end
end

Dir.entries("#{exercise_path}").each do |entry|
  files = Dir["#{exercise_path}/#{entry}/*.yml"].each do |file|
    begin
      exercises_test << YAML.load_file(file)
    rescue
      byebug
    end
  end
end

exercises_test.each do |exercise|
  exercise['content'] = YAML.load(exercise['content'], symbolize_names: true).to_json

  if exercise['type'] == 'multiple_choice'
    content = JSON.parse(exercise['content'], symbolize_names: true)
    if content[:blanks]
      content[:blanks].each do |blank|
        blank[:suggestions] << blank[:solutions].first
        blank[:suggestions].shuffle!.uniq!
      end
    end
    exercise['content'] = content.to_json
  end
end

exercises_test.uniq! {|e| e['id']}

FileUtils.cp 'final_resources.json', 'final_resources_tmp.json'
FileUtils.cp 'final_exercises.json', 'final_exercises_tmp.json'

File.write('final_resources.json', JSON.pretty_generate(resources_test))
File.write('final_exercises.json', JSON.pretty_generate(exercises_test))
