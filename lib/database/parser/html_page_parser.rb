require 'pp'
require 'nokogiri'
require 'net/http'

module HtmlPageParser
  def self.parse_html_page(url)
    begin
      page = Nokogiri::HTML(open(url, read_timeout: 10, open_timeout: 10))
    rescue => e
      puts "RETRY! URL not found: #{url}"
      retry
    end

    exercises = []

    content = page.css('.entry-content')

    # id
    id = page.css('article').first.attribute('id').value.split('-', 2)[1]

    # # audio
    # content.css('.audio-wrapper-wrapper').each do |wrapper|
    #   mp3_url = wrapper.css('a/@href').text
    #   audio_control = "<audio controls><source src='#{mp3_url}' type='audio/mpeg'>Your browser does not support the audio element.</audio>"
    #   wrapper.replace audio_control
    # end

    # exercises
    content.css('.exercise').each do |exercise|
      id = exercise.attribute('id').value.split('-', 2)[1]

      # exercise.replace "<div class='exercise' id='exercise-#{id}'></div>"
      exercise.replace '%s'
      exercises << id
    end

    # Vokabeltrainer
    content.css('.cardtrainer-wrapper').each do |cardtrainer|
      id = cardtrainer.attribute('id').value.split('-')[1]
      cardtrainer.replace "<p>[Vokabeltrainer: #{id}]</p>"
    end

    # # remove classes and tags
    # tag_names = %w(.buchstabenraten-quiz .abuser-content a script)
    # tag_names.each do |tag_name|
    #   content.css("#{tag_name}").remove
    # end
    #
    # # remove language
    # # ToDo
    #
    # # filter stuff
    # content.css('a').remove
    # content.css('script').remove

    { id: id, content: content.to_xhtml, exercises: exercises }
  end
end
