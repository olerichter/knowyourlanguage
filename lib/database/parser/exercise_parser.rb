require 'pp'

module ExerciseParser
  BLANK_TOKEN = '%s'.freeze

  def self.parse_exercise(exercise)
    content, config = exercise.split('####')

    unless content

    end

    unless config
      config = exercise
      content = 'NO CONTENT!'
    end

    content = clean_content content

    config = config.lines.map(&:chomp).delete_if(&:empty?)
    e = { content: content, config: config }

    case config[0]
    when '+1. TextSpotting'
      content = parse_text_spotting e
      type = 'text_spotting'
    when '+1. MultipleChoice'
      content = parse_cloze_and_multiple_choice e
      type = 'multiple_choice'
    when '+1. Cloze'
      content = parse_cloze_and_multiple_choice e
      type = 'cloze'
    when '+1. DragDropList'
      content = parse_drag_drop_list e
      type = 'drag_drop_list'
    when '+1. DragDrop'
      content = parse_drag_drop e
      type = 'drag_drop'
    when '+1. Wordlist'
      content = parse_wordlist e
      type = 'wordlist'
    else
      content = parse_unknown e
      type = 'unknown'
    end

    c = parse_config config
    { content: content.to_json, config: c, type: type }
  end

  private

  def self.clean_content(content)
    content = content.gsub '[[arrow]]', '→'

    content
  end

  def self.parse_wordlist(e)
    content = e[:content]
    words = []
    content.split("\n\n").each do |w|
      next if w.empty?

      enslish = nil
      description = nil
      example = nil
      german = nil

      if w.lines.select {|l| l.start_with? 'translation:'}.any?
        english = w.lines[0].split('#').first.split('/').first.strip       
        w.lines.each do |l|
          if l.start_with? 'ex:'
            description = l.split(":")[1]
            example = l.split(":")[1]
          elsif l.start_with? 'translation:'
            german = l.split(":")[1].split('#').first.split('/').first.strip
          end
        end
      else
        english = w.lines[0].split('#').first.split('/').first.strip
        description = w.lines[1].strip
        example = w.lines[2].strip
        german = w.lines[3].strip
      end

      byebug if english.nil? or german.nil?

      words << { english: english, german: german, description: description, example: example }      
    end
    { words: words }
  end

  def self.parse_config(c)
    result = {}
    result[:feedback] = c.select { |c| c.include? 'furtherFeedback' }.first
    result[:orf] = c.select { |c| c.include? 'orf' }.first
    result[:type] = c.select { |c| c.include? '+1' }.first
    result[:options] = c.select { |c| c.include? 'options' }.first

    result
  end

  def self.parse_unknown(e)
    puts "X Unknown exercise-type: #{e[:config][0]}"
  end

  def self.parse_text_spotting(e)
    content = (e[:content].split(/===[\W]*===/).map { |e| e.split("\n").delete_if { |i| i.empty? || i.eql?('===') } })

    words = []
    content.each do |e|
      if e[0].include? '@'
        word = e[0].split('@').last
        correct = true
      else
        word = e[0]
        correct = false
      end

      words << { word: word, correct: correct }
    end

    { words: words }
  end

  def self.parse_cloze_and_multiple_choice(e)
    includes_blank_token = e[:content].include? '[[blankInput]]'

    e[:content].gsub! '[[blankInput]]', BLANK_TOKEN if includes_blank_token

    text = ''
    blanks = []

    parts = e[:content].split('===')

    parts.each_with_index do |part, index|
      # part is text
      if index.even?
        part = part.gsub /\n[\n]+/, '<br />'
        part = part.gsub '---', '<br />'

        text += part
      # part is blank
      else
        text += BLANK_TOKEN unless includes_blank_token
        blank = {}

        # remove empty lines
        part = part.gsub /^$\n/, ''

        part.lines.each do |l|
          next if l.chomp.strip.empty?

          if l.start_with? '#c'
            blank[:feedback_correct] = l[2..-1]
          elsif l.start_with? '#i'
            blank[:feedback_wrong] = l[2..-1]
          elsif l.include? '/'
            l.split('/').each do |sl|
              if sl.start_with? '+'
                (blank[:solutions] ||= []) << sl[1..-1]
              elsif sl.start_with? '-'
                (blank[:suggestions] ||= []) << sl[1..-1]
              end
            end
          elsif l.start_with? '+'
            (blank[:solutions] ||= []) << l[1..-1]
          elsif l.start_with? '-'
            (blank[:suggestions] ||= []) << l[1..-1]
          elsif !l.start_with? '#', '-', '+'
            (blank[:solutions] ||= []) << l
          end
        end
        blanks << blank
      end
    end

    { text: text, blanks: blanks }
  end

  def self.parse_drag_drop_list(e)
    categories = e[:content].split('{{')

    words = []
    category_names = []

    categories.each do |c|
      next if c.strip.empty?

      category_name = c.lines[0][2..-3].tr('{', '').tr('#', '').tr('}', '')
      category_names << category_name

      c = c.split("\n")[1..-1].join("\n")

      c.split("\n\n").each do |w|
        word = {}
        w.lines.each do |l|
          l.chomp!
          next if l.strip.empty?
          if l.start_with? '#c'
            word[:feedback_correct] = l[2..-1]
          elsif l.start_with? '#i'
            word[:feedback_wrong] = l[2..-1]
          elsif !l.start_with? '#'
            word[:word] = l
          end
        end
        word[:category] = category_name
        words << word
      end
    end

    { categories: category_names, words: words }
  end

  def self.parse_drag_drop(e)
    result = []

    e[:content].split("\n\n").each do |e|
      element = {}
      lines = e.lines.select { |l| !l.empty? || !l.strip.empty? }
      next if lines.empty?
      element[:question] = lines[0].strip
      element[:answer] = lines[1].strip

      lines.each do |l|
        if l.start_with? '#c'
          element[:feedback_correct] = l[2..-1]
        elsif l.start_with? '#i'
          element[:feedback_wrong] = l[2..-1]
        end
      end

      result << element
    end

    result
  end

  # def self.parse_multiple_choice e
  #   questions = e[:content].split("===\n\n")
  #
  #   result = []
  #
  #   questions.each do |q|
  #     next if q.strip.empty?
  #     question = {}
  #     question[:suggestions] = []
  #
  #     q.lines.each do |l|
  #       l.chomp!
  #       next if l.strip.empty?
  #
  #       if l.start_with? "-"
  #         question[:suggestions] << l[1..-1]
  #       elsif l.start_with? "+"
  #         question[:solution] = l[1..-1]
  #         question[:suggestions] << l[1..-1]
  #       elsif l.start_with? '#c'
  #         question[:feedback_correct] = l[2..-1]
  #       elsif l.start_with? '#i'
  #         question[:feedback_wrong] = l[2..-1]
  #       elsif l.match(/^[a-zA-Z]/) || l.start_with?("[[") || l.start_with?("%s")
  #         question[:text] = l.gsub!("[[blankInput]]", "%s") || "#{l}%s"
  #       end
  #     end
  #     result << question
  #   end
  #
  #   result
  # end

  # def self.parse_cloze e
  #   if !e[:content].include? "---"
  #     raise "cloze test syntax error!!!!!"
  #   end
  #
  #   questions = e[:content].split("---")
  #
  #   result = []
  #
  #   questions.each do |q|
  #     next if q.strip.empty?
  #     question = {}
  #
  #     first, cloze, last = q.split "==="
  #     question[:text] = "#{first.strip} #{BLANK_TOKEN} #{last.strip}"
  #
  #     cloze.lines.each do |l|
  #       l.chomp!
  #       if l.start_with? '#c'
  #         question[:feedback_correct] = l[2..-1]
  #       elsif l.start_with? '#i'
  #         question[:feedback_wrong] = l[2..-1]
  #       elsif !l.start_with? '#'
  #         question[:solution] = l
  #       end
  #     end
  #     result << question
  #   end
  #
  #   result
  # end
end
