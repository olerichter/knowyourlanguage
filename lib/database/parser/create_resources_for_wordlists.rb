require 'nokogiri'
require 'JSON'
require 'open-uri'
require 'pp'
require 'byebug'
require 'YAML'
require 'cgi'

final_exercises = JSON.parse(File.read('final_exercises.json'), symbolize_names: true)

wordlist_exercises = final_exercises.select {|e| e[:type] == 'wordlist'}.uniq { |e| e[:name] }
wordlist_resources = []
wordlist_exercises.each do |exercise|
  exercise[:id] = "1" + exercise[:id]

  content = "<div>%s</div>"
  id = "#{rand.to_s[2..33]}#{rand.to_s[2..33]}"

  resource = {
    id: id,
    name: "Wortschatz: #{exercise[:name]}",
    unit_name: "Wortschatz - Wortschatz",
    exercises: [exercise[:id]],
    content: content
  }

  wordlist_resources << resource
  puts '.'
end

File.write('wordlist_exercises.json', JSON.pretty_generate(wordlist_exercises))
File.write('wordlist_resources.json', JSON.pretty_generate(wordlist_resources))

