require 'nokogiri'
require 'JSON'
require 'open-uri'
require 'pp'
require 'byebug'
require 'YAML'
require 'cgi'

class ExerciseBeautifier
  def initialize(exercise)
    @exercise = exercise
  end

  def update_manually_edited_exercises
    exercise_data = YAML.load_file('manually_edited_exercises.yml', symbolize_names: true).select { |e| e['id'].to_s[0..10] == @exercise[:id][0..10] }.first

    if exercise_data
      @exercise[:type] = exercise_data['type']
      @exercise[:content] = exercise_data['content'].to_json
    end

    @exercise
  end

  def beautify
    # shuffle drag_drop
    if @exercise[:type] == 'drag_drop' && @exercise[:id] == '24373811362061940000011493074829'
      @exercise[:content] = JSON.parse(@exercise[:content]).shuffle.to_json
    end

    # get skills and name
    exercise_data = YAML.load_file('structure_exercises.yml').select { |e| e['id'].to_s[0..10] == @exercise[:id][0..10] }.first

    skills = []

    if exercise_data
      name = exercise_data['name']
      niveau = exercise_data['niveau']
      level = exercise_data['level']
      
      exercise_data['skills'].each do |s|
        skill = {}
        skill[:category] = s['category']
        skill[:name] = s['topic']
        skills << skill
      end
    else
      puts "no data in yml (#{@exercise[:id]}"
    end

    @exercise[:name] = name
    @exercise[:skills] = skills
    @exercise[:niveau] = niveau
    @exercise[:level] = level
    

    @exercise
  end
end

################### MAIN ###################

exercises = JSON.parse(File.read('final_exercises_pure.json'), symbolize_names: true)

exercises.each do |exercise|
  exercise = ExerciseBeautifier.new(exercise).beautify
  exercise = ExerciseBeautifier.new(exercise).update_manually_edited_exercises
  
  puts '.'
end

File.write('final_exercises.json', JSON.pretty_generate(exercises))
