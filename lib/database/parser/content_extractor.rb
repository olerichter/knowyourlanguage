require 'fileutils'
require 'JSON'
require 'YAML'
require 'byebug'

def slugify name
  name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
end

# puts 'zurück in den Content-Ordner schreiben???'
# exit


resources = JSON.parse(File.read('final_resources.json'))
exercises = JSON.parse(File.read('final_exercises.json'))

exercise_path = 'content/exercises'
resource_path = 'content/resources'

FileUtils::rm_r resource_path
FileUtils::rm_r exercise_path

FileUtils::mkdir_p resource_path
FileUtils::mkdir_p exercise_path

resources.each_with_index do |resource, index|
  resource['content'].tr!("\n","")
  resource['content'].tr!("\t","")
  resource['content'].gsub!(/ +/, " ")

  path = "#{resource_path}/#{slugify resource["unit_name"]}"
  filename = slugify "#{index.to_s.rjust(3, "0")}_#{resource['name']}"

  FileUtils::mkdir_p path
  File.open("#{path}/#{filename}.yml", 'w') {|f| f.write resource.to_yaml }
end

exercises.each_with_index do |exercise, index|

  path = "#{exercise_path}"
  filename = slugify "#{index.to_s.rjust(3, "0")}_#{exercise['name']}_#{exercise['id']}"

  exercise['content'] = JSON.load(exercise['content'].strip).to_yaml
  
  FileUtils::mkdir_p path
  File.open("#{path}/#{filename}.yml", 'w') {|f| f.write exercise.to_yaml }
end
