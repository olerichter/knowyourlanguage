require 'nokogiri'
require 'JSON'
require 'open-uri'
require 'pp'
require_relative 'exercise_parser'
require_relative 'html_page_parser'
require 'byebug'
require 'YAML'
require 'cgi'

def resource_type_of(resource)
  words = %w(multiLangContent contentSnippet contentInclude contentInput)
  if words.any? { |word| resource[:resource_content].include?(word) }
    return 'resource'
  else
    return 'other'
  end
end

def parse_exercises(doc)
  exercises = []
  doc.xpath('//contentInput').each do |t|
    exercise = {}
    exercise[:id] = t.attribute('uniqueId').value
    exercise[:content_reference_ids] = t.attribute('contentReferenceUniqueIds').value if t.attribute('contentReferenceUniqueIds')
    exercise[:type] = t.attribute('type').value
    exercise[:content] = t.inner_html

    # filter resources with same id
    exercises << exercise unless exercises.any? { |e| e[:id] == exercise[:id] }
  end

  # insert content references
  exercises.each do |exercise|
    if exercise[:content_reference_ids]
      first_id = exercise[:content_reference_ids].split(',').first
      exercise[:content] = exercises.select { |e| e[:id] == first_id }.first[:content]
    end
  end

  exercises
end

# :id, :content, :exercises, :name
def fetch_resource_by_url(url, _doc)
  HtmlPageParser.parse_html_page url
end

# [:id, :type, :content, :config]
def fetch_exercises_by_id(final_resource, exercises, existing_exercises)  
  return [] if final_resource[:exercises].nil?

  final_resource[:exercises].map do |id|
    # keep old exercise if existing
    existing_exercise = existing_exercises.select { |e| e[:id].to_s[0..10] == id.to_s[0..10] }.first
    if existing_exercise
      parsed_exercise = existing_exercise 
      puts "keep Exercise: #{parsed_exercise[:type]}"
    else
      exercise = exercises.select { |exercise| exercise[:id].start_with? id[0, 10] }.first

      byebug if exercise.nil?

      byebug unless exercise[:content]

      parsed_exercise = ExerciseParser.parse_exercise exercise[:content]

      byebug unless parsed_exercise

      puts "new Exercise: #{parsed_exercise[:type]}"
    end

    # get skills and name
    exercise_data = YAML.load_file('structure_exercises.yml').select { |e| e['id'].to_s[0..10] == id[0..10] }.first

    skills = []

    if exercise_data
      name = exercise_data['name']

      exercise_data['skills'].each do |s|
        skill = {}
        skill[:category] = s['category']
        skill[:niveau] = s['niveau']
        skill[:name] = s['topic']
        skills << skill
      end
    else
      puts 'no skill in yaml'
    end

    {
      id: id,
      type: parsed_exercise[:type],
      content: parsed_exercise[:content],
      config: parsed_exercise[:config],
      skills: skills,
      name: name
    }
  end
end

################### MAIN ###################

structure = YAML.load_file('structure_resources.yml')
doc = File.open('wp_20_posts.xml') { |f| Nokogiri::XML(f) }

existing_resources = JSON.parse(File.read('final_resources_pure.json'), symbolize_names: true)
existing_exercises = JSON.parse(File.read('final_exercises_pure.json'), symbolize_names: true)

existing_exercises = []
files = Dir["additional_content/*_exercises.json"].each do |file|
  existing_additional_exercises = JSON.parse(File.read(file), symbolize_names: true)
  existing_exercises += existing_additional_exercises
end

files = Dir["additional_content/*_exercises.yml"].each do |file|
  existing_additional_exercises = YAML.load_file(file).map(&:symbolize_keys!)
  existing_exercises += existing_additional_exercises
end


exercises = parse_exercises doc

final_resources = []
final_exercises = []

structure.each do |part, resources|
  resources.each do |resource, _i|
    final_resource = fetch_resource_by_url(resource['url'], doc)

    # keep old resource if existing
    existing_resource = existing_resources.select { |r| r[:id] == final_resource[:id] }.first
    final_resource = existing_resource if existing_resource
      
    final_resource[:name] = CGI.unescapeHTML(resource['name']) if resource['name']
    final_resource[:unit_name] = part
    final_resource[:type] = resource[:type]
    
    final_resources << final_resource

    puts "Resource: #{final_resource[:name]}"

    final_exercises += fetch_exercises_by_id(final_resource, exercises, existing_exercises)
  end
end

File.write('final_exercises_pure.json', JSON.pretty_generate(final_exercises))
File.write('final_resources_pure.json', JSON.pretty_generate(final_resources))

puts 'the end'
